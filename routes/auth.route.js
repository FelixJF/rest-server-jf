const {Router} = require('express')
const { check } = require('express-validator')
const { login, google } = require('../controllers/auth.controller')
const { validationError } = require('../middlewares/validatorErrors')
const router = Router()

router.post('/login',[
    check('correo', 'El correo es obligatorio').isEmail(),
    check('password', 'La contraseña es obligatoria').notEmpty(),
    validationError
], login)

router.post('/google',[
    check('id_token', 'id_token es necesario').notEmpty(),
    validationError
], google)


module.exports = router