const {Router} = require('express')
const { check } = require('express-validator')
const { usuariosGet,
        usuariosPut, 
        usuariosPost, 
        usuariosDelete } = require('../controllers/user.controller')

const { esRoleValido, 
        emailExiste, 
        userExiste} = require('../helpers/db-validators')
       
const { 
    validaRoleAdmin,
    tieneRole,
    validationError,
    validarJWT
} = require('../middlewares/index')

const router = Router()

router.get('/', usuariosGet)

router.put('/:id', [
    check('id','No es un ID valido').isMongoId(),
    check('id').custom(userExiste),
    check('rol').custom( esRoleValido),
    validationError
], usuariosPut)

router.post('/',[
    check('correo','Este no es un correo valido').isEmail().bail().custom(emailExiste),
    check('nombre','El nombre es obligatorio').not().isEmpty(),
    check('password','El password es obligatorio').not().isEmpty().bail().isLength({min:6}).withMessage('El password debe tener mas de 6 letras') ,
    check('rol').custom( esRoleValido),
    // Esto se puede obviar con la linea anterior... Ya que es solo un parametro
    // check('rol').custom( (rol) => esRoleValido(rol)),
    validationError
], usuariosPost)

router.delete('/:id',[
    validarJWT,
    // validaRoleAdmin,
    tieneRole('USER_ROLE'),
    check('id','No es un ID valido').isMongoId(),
    check('id').custom(userExiste),
    validationError
], usuariosDelete)

module.exports = router;