const express = require('express')
const cors = require('cors')

const {dbConnection} = require('../database/config')

class Server {
    constructor() {
        this.app = express()
        this.port = process.env.PORT
        this.usuariosPath = '/api/usuarios'
        this.authPath = '/api/auth'
        //Conectar a base de datos
        this.conectarDB();
        //Middlewares
        this.middlewares();
        //Rutas de mi app
        this.routes()
    }
    async conectarDB(){
        //Aca podemos establcer si queremos conectarnos a la dev o prod, y asi cambiar entre BD
        await dbConnection();

    }
    routes() {
        this.app.use(this.authPath, require('../routes/auth.route'))
        this.app.use(this.usuariosPath, require('../routes/user.route'))
        this.app.put('*', (req, res) => {
            res.send('RUTA NO ENCONTRADA')
        })
        
    }
    listen(){
        this.app.listen(this.port, ()=>{
            console.log(`Servidor corriendo en http:localhost:${this.port}/`)
        })
    }
    middlewares(){
        //Cors
        this.app.use( cors() )
        //Parseo y lectura de body 
        this.app.use(express.json())
        //Directorio publico
        this.app.use( express.static('public'))
    }

}
module.exports = Server;