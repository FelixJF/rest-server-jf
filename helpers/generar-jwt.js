const jwt = require('jsonwebtoken')

const generarJWT = (uid = '')=> {
    return new Promise ((resolve, reject)=>{
        //Este es la data que queremos que el token encripte, el SECRETOPRIVATE no se debe conocer al publico, pues es la firma 
        //con la que se generan los token de nuestros users
        const payload = {uid, sofenix:'sofenix'}
        jwt.sign(payload,process.env.SECRETORPRIVATEKEY,{
            expiresIn:'4h',
        }, (err, token)=> {
            if(err){
                console.log(err)
                reject('No se pudo generar token')
            }else{
                resolve(token)
            }
        })
    })

}

module.exports = {
    generarJWT

}