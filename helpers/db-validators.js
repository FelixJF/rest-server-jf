const Role = require("../models/role");
const Usuario = require("../models/usuario");

const esRoleValido = async (rol='')=> {
    const existeRol = await Role.findOne({rol});
    if(!existeRol){
        throw new Error(`EL rol ${ rol } no esta registrado en la BD`)
    }
}
const emailExiste = async(correo = '') => {
    console.log(correo)
    const existeEmail = await Usuario.findOne({correo})
    if(existeEmail){
        throw new Error(`EL correo ${ correo } ya existe`)
    }

}
const userExiste = async(id = '') => {
    const existeUser = await Usuario.findById({_id:id})
    console.log('existe')
    if(!existeUser){
        throw new Error(`No existe usuario con el ID ${ id }.`)
    }

}

module.exports = {
    esRoleValido,
    emailExiste, 
    userExiste
}