const { validationResult } = require("express-validator")


const validationError = (req, res, next) => {
    const validation = validationResult(req)
    if(!validation.isEmpty()){
        return res.status(400).json(validation)
    }
    next()
}

module.exports = {
    validationError
}