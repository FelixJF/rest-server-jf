const { response, request } = require('express')
const jwt = require('jsonwebtoken')
const Usuario = require('../models/usuario')

const validarJWT = async (req = request, res = response, next) => {
    //Capturamos el token que viene en el header, lo validamos
    const token = req.header('x-token')
    if(!token){
        return res.status(401).json({
            msg:'Sin token en la peticion'
        })
    }
    //Si pasa, entonces lo decodificamos, 
    //y asi obtenemos el payload que grabamos en el al generar el token
    try {
        const {uid} = jwt.verify(token, process.env.SECRETORPRIVATEKEY) 
        usuario = await Usuario.findById(uid)
        //Validando que user este en bd
        if(!usuario){
            return res.status(401).json({
                msg:'Token no valido - Usuario no esta en base de datos'
            })
        }
        //Validando que el user autenticado no este borrado
        if(!usuario.estado){
            return res.status(401).json({
                msg:'Token no valido - Sin acceso'
            })
        }
        req.usuario = usuario
        next()
    } catch (error) {
        console.log(error)
        return res.status(401).json({
            msg:'Token no valido'
        })
    }

}

module.exports = {
    validarJWT
}