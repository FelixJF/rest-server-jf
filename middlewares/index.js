        
const validaJWT   = require('../middlewares/validar-jwt')
const validaRoles = require('../middlewares/validar-roles')
const validaError = require('../middlewares/validatorErrors')

module.exports = {
    ...validaError,
    ...validaJWT,
    ...validaRoles
}

