const { response } = require("express")


const validaRoleAdmin = (req, res = response, next) => {
    if(!req.usuario){
        return res.status(500).json({
            msg:'No hay usuario validado en el request'
        })
    }
    if(req.usuario.rol !== 'ADMIN_ROLE'){
        return res.status(400).json({
            msg:'No tiene permisos de admin para realizar esta accion'
        })
    }
    next()
}
const  tieneRole = (...roles)=>{

    return (req, res, next) => {
        if(!req.usuario){
            return res.status(500).json({
                msg:'No hay usuario validado en el request'
            })
        }
        if(!roles.includes(req.usuario.rol)){
            return res.status(401).json({
                msg:`El servicio requiere roles :${roles}`
            })
        }
        next()
    }


}

module.exports = {
    validaRoleAdmin,
    tieneRole
}