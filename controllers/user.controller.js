const { response, request, query } = require('express')
const bcryp = require('bcryptjs')
const Usuario = require('../models/usuario')
const { validationResult } = require('express-validator')

const usuariosGet = async (req = request, res = response) => {
    const {limite = 3, desde = 0}  = req.query
    query_ = {estado:true}
    //Lo colocamos de la siguiente forma como un Promise All, 
    //para que se ejecuten todo lo que queremos de promesa, y solo hasta que termine todo, enviamos
    //la info === Igual hacemos desestructuracion de ARREGLOS 
    const [total, usuarios] = await Promise.all([
        Usuario.countDocuments(query_),
        Usuario.find(query_)
        .skip(desde)
        .limit(limite)
    ])
    res.json({
        total,
        usuarios
    })
}

const usuariosPost = async(req = request, res = response) => {
    
    const {nombre, correo, password, rol} = req.body
    const usuario = new Usuario({nombre, correo, password, rol})
    
    //Encriptar contraseña
    const salt = bcryp.genSaltSync(10);
    usuario.password = bcryp.hashSync(password, salt)
    
    //Guardar en la bd
    await usuario.save()

    res.json({
        ok: true,
        usuario        
    })
}

const usuariosPut = async(req = request, res = response) => {
    const {id} = req.params
    const {_id, password, google, correo, ...resto } = req.body
    //Si password viene, entonces quiere decir que hay que 
    //actualizar, y lo agregamos al 'resto'
    if(password){
        const salt = bcryp.genSaltSync(10);
        resto.password = bcryp.hashSync(password, salt)
    }
    const usuario = await Usuario.findByIdAndUpdate(id, resto)


    res.json({
        ok: true,
        msj: 'PUT API - controlador',
        usuario
    })
}

const usuariosDelete = async(req, res = response) => {
    const {id} = req.params
    const  usuario =  await Usuario.findByIdAndUpdate(id, {estado:false})
    //Cuando mandamos req.usuario, implicitamente se ejecuta .toJSON, el cual se esta en el modelo sobreescrito
    res.json({usuario})
}

module.exports = {
    usuariosGet,
    usuariosDelete,
    usuariosPut,
    usuariosPost
}